import Vue from 'vue'
import Router from 'vue-router'

import AuthStore from '@/common/auth.store'

import Welcome from '@/views/Welcome'
import Login from '@/views/Login'
import Home from '@/views/Home'
import Timeline from '@/views/Timeline'
import Profile from '@/views/Profile'
import Post from '@/views/Post'

Vue.use(Router)

const authenticatedRoutes = ['home', 'timeline', 'post', 'profile']

const router = new Router({
  routes: [
    {
      path: '/',
      name: 'welcome',
      component: Welcome
    },
    {
      path: '/login',
      name: 'login',
      component: Login
    },
    {
      path: '/home',
      name: 'home',
      component: Home,
      children: [
        {
          path: '',
          name: 'timeline',
          component: Timeline
        },
        {
          path: '/post',
          name: 'post',
          component: Post
        },
        {
          path: '/profile',
          name: 'my_profile',
          component: Profile
        }
      ]
    },
    {
      path: '/users/:id',
      name: 'others_profile',
      component: Profile
    }
  ]
})

router.beforeEach((to, from, next) => {
  console.log('router to', to)
  console.log('router from', to)
  console.log('auth', AuthStore.getCredentials())
  if (authenticatedRoutes.some((element, index, array) => {
    return element === to.name
  }) && !AuthStore.getCredentials().token) {
    next({ path: '/' })
  }
  next()
})

export default router
