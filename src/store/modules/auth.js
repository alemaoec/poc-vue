import UserResource from '@/api/resources/users'
import AuthStore from '@/common/auth.store'

const state = {
  errors: null,
  user: {},
  isAuthenticated: !!AuthStore.getCredentials()
}

const getters = {
  currentUser (state) {
    debugger
    return state.user
  },
  isAuthenticated (state) {
    return !!state.user.token
  }
}

const actions = {
  login (authStore, credentials) {
    return new Promise((resolve) => {
      UserResource.auth(credentials).then(({data}) => {
        authStore.commit('setUser', data)
        resolve(data)
      }).catch(({response}) => {
        authStore.commit('setError', 'Suas credenciais estão inválidas!')
      })
    })
  }
}

const mutations = {
  setError (state, error) {
    state.errors = error
  },
  setUser (state, user) {
    state.isAuthenticated = true
    state.user = user
    state.errors = {}
    AuthStore.saveCredentials({
      token: state.user.authentication_token,
      email: state.user.email
    })
  }
}

export default {
  state,
  actions,
  mutations,
  getters
}
