// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import Vuetify from 'vuetify'
import VueLazyload from 'vue-lazyload'

import App from '@/App'
import store from '@/store'
import router from '@/router'
import ApiService from '@/api/api.service'

import 'vuetify/dist/vuetify.min.css'
import colors from 'vuetify/es5/util/colors'

ApiService.init()

Vue.config.productionTip = false

Vue.use(Vuex)
Vue.use(VueLazyload)
Vue.use(Vuetify, {
  theme: {
    primary: colors.indigo.darken1,
    secondary: colors.grey.darken3,
    accent: colors.indigo.darken1
  }
})

/* eslint-disable no-new */
new Vue({
  el: '#app',
  store,
  router,
  components: { App },
  template: '<App/>'
})
