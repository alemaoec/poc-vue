import ApiService from '@/api/api.service'

const PostResource = {
  query (params) {
    console.log(params)
    return ApiService
      .setHeader()
      .query('posts', params)
  },
  get (slug) {
    return ApiService.get('posts', slug)
  },
  create (params) {
    return ApiService.post('posts', params)
  },
  update (slug, params) {
    return ApiService.update('posts', slug, params)
  },
  destroy (slug) {
    return ApiService.delete(`posts/${slug}`)
  }
}

export default PostResource
